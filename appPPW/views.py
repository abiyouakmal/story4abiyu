from django.shortcuts import render
from django.shortcuts import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .forms import EventForm
from .models import Event


def education(request):
    return render(request, 'Education.html')


def experience(request):
    return render(request, 'Experiences.html')


def portofolio(request):
    return render(request, 'Portofolio.html')


def form(request):
    return render(request, 'Form.html')


def personal_schedule(request):
    events = Event.objects.all().values()

    if request.method == "POST":
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('form'))
    else:
        form = EventForm()

    response = {'form': form, 'events': events}
    return render(request, 'Form.html', response)


def delete_all_events(request):
    if request.method == "POST":
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('form'))

    events = Event.objects.all().delete()
    form = EventForm()
    response = {'form': form, 'events': events}
    return render(request, 'Form.html', response)
