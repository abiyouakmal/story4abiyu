from django import forms
from .models import Event
from django.forms import ModelForm


class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ['name', 'category', 'location', 'date', 'time']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Isi nama'}),
            'location': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'isi tempat'}),
            'date': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'time': forms.TimeInput(attrs={'class': 'form-control', 'type': 'time'}),
            'category': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'isi kategori'}),
        }

        labels = {
            'name': 'Nama Kegiatan',
            'location': 'Tempat Kegiatan',
            'date': 'Tanggal Kegiatan',
            'time': 'Jam Kegiatan',
            'category': 'Kategori Kegiatan',
        }
