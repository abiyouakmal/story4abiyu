from django.shortcuts import render
from django.test import TestCase, Client
from django.urls import resolve
from requests import request

from appPPW.forms import EventForm
from appPPW.models import Event
from .views import education, experience, personal_schedule, form, portofolio


class Lab3Test(TestCase):
    def test_story8_url_HOMEorEDUCATION_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_EXPERIENCES_is_exist(self):
        response = Client().get('/experience/')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_PORTOFOLIO_is_exist(self):
        response = Client().get('/portofolio/')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_MYSCHEDULE_is_exist(self):
        response = Client().get('/form/')
        self.assertEqual(response.status_code, 200)

    def test_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/form/', {'name': test, 'category': test, 'location': test, 'date': '1999-07-31',
                                                 'time': '00:00'})
        self.assertEqual(response_post.status_code, 302)  # 302 = Redirect

    # def test_delet_all_post_button(self):
    #     new_act = Event.objects.create(name='abiyu', category='ganteng',
    #                                    location='hatimu', date='1999-07-31', time='00:00')
    #     events = Event.objects.all().delete()
    #     form = EventForm()
    #     response = {'form': form, 'events': events}
    #     render(request, 'Form.html', response)
    #     self.assertEquals()

    def test_story8_formHTML_rendered(self):
        response = self.client.get('/form/')
        self.assertTemplateUsed(response, 'Form.html')

    def test_url_is_not_Exsist(self):
        response = Client().get('kakpewe/')
        self.assertEqual(response.status_code, 404)
