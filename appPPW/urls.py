from django.urls import path
from django.urls import path
from appPPW import views

# from .views import *

urlpatterns = [
    path('experience/', views.experience, name='experience'),
    path('portofolio/', views.portofolio, name='portofolio'),
    path('form/', views.personal_schedule, name='form'),
    path('delete_all_events/', views.delete_all_events, name='delete_all_events'),
    path('', views.education, name='education'),
]
